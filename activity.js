/*
	1. What directive is used by Node.js in loading the modules it needs?

		require


	2. What node.js module contains a method for server creation?

	(function (request, response)


	3. What is the method of the http object responsible for creating a server using Node.js?

	http.createServer
	
	4. What method of the response object allows us to set status codes and content types?

	response.writeHead


	5. Where will console.log() output its content when run in Node.js?

	terminal or bash


	6. What property of the request object contains the address' endpoint?

	.listen()


*/